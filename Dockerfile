FROM python:bullseye AS base

COPY ["requirements.txt", "hello.py", "./"]
run pip install -r requirements.txt 

FROM base AS runtime
ENTRYPOINT ["python", "hello.py"]
